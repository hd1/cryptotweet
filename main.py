import argparse
import cStringIO as StringIO
import datetime
import json
import logging
import os
import subprocess
import tempfile

from bottle import get, post, request, run, static_file, HTTPError
from bs4 import BeautifulSoup
import requests
import twitter

GNUPG_HOME = '/home/ubuntu/.gnupg'

# rertieve recipient's key from HKP server, if not found
def get_key(key):
    key = key.replace(' ','')
    if not key.startswith('0x'):
        key = '0x'+key
    result = subprocess.check_output(['/usr/bin/gpg', '--recv-key', receipient, '--keyserver', 'pool.sks-keyservers.net'], stderr=subprocess.STDOUT) 
    if result.find('skipping') != -1:
        logging.debug('No key found')
        raise HTTPError(body='Key retrieval encountered an error')
    try:
        ret = subprocess.check_output(['/usr/bin/gpg', '--export', '-a', recipient])
    except Exception, e:
        logging.error(str(e))
        raise HTTPError(body='Error exporting key {0}'.format(recipient), exception=e)

    return ret

def encrypt_string(unencrypted_string, recipient):
    unencrypted_file = tempfile.NamedTemporaryFile(prefix='cryptotweets', delete=False, suffix='raw')
    try:
        unencrypted_file.write(unencrypted_string)
        stream = subprocess.check_output(["/usr/bin/gpg", "-e", "-a", '-r', recipient], stdin=unencrypted_file, close_fds=True)
        logging.debug('Crypted message: {0}'.format(stream))
        return stream
    except Exception, e:
        logging.error(str(e))
        raise HTTPError(body='Encryot error: {0}'.format(str(e)), exception=e)
    finally:
        os.unlink(unencrypted_file.name)

def create_gist(gist_body, recipient):
    url = "https://api.github.com/gists"
    description = 'message for {0}, received on {1}'.format(recipient, datetime.datetime.now().strftime('%s'))
    payload = {'description': description, 'public': 'true', 'files': {'file1.txt.asc': {'content': gist_body}}}
    logging.debug('Parameers to create gist: {0}'.format(payload))
    headers = { 'content-type': "application/json", 'authorization': "Basic aGRpd2FuOlJOUXFEemZIeHR3eFJPbWZlczZiaWM=", 'cache-control': "no-cache", 'postman-token': "9a7be9c5-9154-af32-6b28-bf903e2b571f" }
    respSource = requests.request("POST", url, json=payload, headers=headers)
    logging.debug(sorted(dir(respSource)))
    resp = respSource.json()
    logging.debug('Gist creation results in {0}'.format(resp))
    raw_url = resp['files']['file1.txt.asc']['raw_url']
    return raw_url

def make_tweet(twitter_recipient, gist_url):
    TOKEN = 'VLxXyzOjpxXnPxjOgFx9wlJNP'
    TOKEN_KEY = 'Ltep5v6jpEilVGZwuItbkS5sS3yjUmckaZc23ZrIPqj21ljwQr'
    CON_SEC = '6148562-w2ugBQPohTbTsCgyH56DWIBCYx4htDXkJoccyEssaV'
    CON_SEC_KEY = 'Ik3XpxhRxksxX8Z2tiPT3NvlaESJ9ftpq2JK3MEthH69q'

    my_auth = twitter.OAuth(CON_SEC, CON_SEC_KEY, TOKEN, TOKEN_KEY)
    twit = twitter.Twitter(auth=my_auth)
    status = 'Hey, @{0}, you have a new priv8tweet, at {1} #priv8tweet'.format(twitter_recipient, gist_url)
    logging.debug(status)
    twit.statuses.update(status=status)

@post('/')
def index():
    params = request.params
    logging.debug('parameters to post: {0} and their values {1}'.format(params.keys(), params.values()))
    enc_string = encrypt_string(params['message'], params['recipientKeyId'])
    gist_location = create_gist(enc_string, params['twitterId'])
    logging.debug('create_gist returns {0}'.format(gist_location))
    make_tweet(params['twitterId'], gist_location)
    return gist_location

@get('/<filename>')
def get_file(filename):
    if filename.find('qr') != -1:
        return static_file('qr.png', root='/home/ubuntu/cryptotweet/static')
    return static_file(filename, root='/home/ubuntu/cryptotweet/static')

@get('/')
def root():
    return static_file('index.html', root='/home/ubuntu/cryptotweet/static')

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(lineno)d- %(message)s')
    logging.debug('Testing logging')
    run(host='0.0.0.0', port=58786, debug=True)
